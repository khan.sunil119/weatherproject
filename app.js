const express = require("express");
const https = require("https");

const app = express();

app.get('/', function(req, res){

    const url = "https://v2.jokeapi.dev/joke/Programming?blacklistFlags=political";
    https.get(url, function(response){
        response.on('data',function(data){
            const jokesData = JSON.parse(data);
            res.write("<h1>The joke category is " + jokesData.category + " </h1>");
            res.write("<h1>The joke type is " + jokesData.type + " </h1>");
            res.send();

        });
    });
});


app.listen(3000, function(){
    console.log('Server is running on 3000 port');
});